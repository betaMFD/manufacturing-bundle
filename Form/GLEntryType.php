<?php

namespace BetaMFD\ManufacturingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class GLEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('reference');
        $builder->add('date');
        $builder->add('memo');
        $builder->add('row1', GLRowType::class, array('label'=>'1'));
        $builder->add('row2', GLRowType::class, array('label'=>'2'));
        $builder->add('row3', GLRowType::class, array('label'=>'3'));
        $builder->add('row4', GLRowType::class, array('label'=>'4'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'BetaMFD\ManufacturingBundle\Entity\GeneralLedgerEntry', ));
    }
}
