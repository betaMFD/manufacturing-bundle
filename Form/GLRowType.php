<?php

namespace BetaMFD\ManufacturingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GLRowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('account');
        $builder->add('dr');
        $builder->add('cr');
        $builder->add('reference');
        $builder->add('memo');
        $builder->add('vendor');
        $builder->add('item');
        $builder->add('inventory');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow', ));
    }
}
