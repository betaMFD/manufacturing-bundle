

This bundle is primarily for my own purposes and as of this writing, is VERY WIP. It's not really usable.


Composer install directions deliberately left out. Will add in later when this is meant to be used.



### Enable the Bundle (Symfony 2.8)

Enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new BetaMFD\ManufacturingBundle\ManufacturingBundle(),
        );

        // ...
    }

    // ...
}
```

### Enable the Bundle In Symfony 3.4+

Enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:
```php
    BetaMFD\ManufacturingBundle\ManufacturingBundle::class => ['all' => true],
```
