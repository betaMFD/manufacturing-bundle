<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 *
 * @ORM\Table(name="manuf_item")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\ItemRepository")
 */
class Item
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\UnitOfMeasure")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $unitOfMeasure;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Vendor")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $defaultVendor;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $defaultManufacturerPartNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=18, scale=6, nullable=true)
     */
    private $cost;

    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow", mappedBy="item")
     * @ORM\JoinColumn(nullable=true)
     */
    private $glRows;


    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Account")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $assetAccount;


    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Account")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $cogsAccount;


    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Account")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $incomeAccount;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $trackInventory = true;

    public function __construct()
    {
        $this->glRows = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Gl Rows
     *
     * @return mixed
     */
    public function getGlRows()
    {
        return $this->glRows;
    }

    /**
     * Set the value of Gl Rows
     *
     * @param mixed glRows
     *
     * @return self
     */
    public function setGlRows($glRows)
    {
        $this->glRows = $glRows;

        return $this;
    }


    /**
     * Get the value of Default Vendor
     *
     * @return string
     */
    public function getDefaultVendor()
    {
        return $this->defaultVendor;
    }

    /**
     * Set the value of Default Vendor
     *
     * @param string defaultVendor
     *
     * @return self
     */
    public function setDefaultVendor($defaultVendor)
    {
        $this->defaultVendor = $defaultVendor;

        return $this;
    }

    /**
     * Get the value of Cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set the value of Cost
     *
     * @param string cost
     *
     * @return self
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }


    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set the value of Notes
     *
     * @param string notes
     *
     * @return self
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get the value of Url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of Url
     *
     * @param string url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of Default Manufacturer Part Number
     *
     * @return string
     */
    public function getDefaultManufacturerPartNumber()
    {
        return $this->defaultManufacturerPartNumber;
    }

    /**
     * Set the value of Default Manufacturer Part Number
     *
     * @param string defaultManufacturerPartNumber
     *
     * @return self
     */
    public function setDefaultManufacturerPartNumber($defaultManufacturerPartNumber)
    {
        $this->defaultManufacturerPartNumber = $defaultManufacturerPartNumber;

        return $this;
    }

    /**
     * Get the value of Asset Account
     *
     * @return integer
     */
    public function getAssetAccount()
    {
        return $this->assetAccount;
    }

    /**
     * Set the value of Asset Account
     *
     * @param integer assetAccount
     *
     * @return self
     */
    public function setAssetAccount($assetAccount)
    {
        $this->assetAccount = $assetAccount;

        return $this;
    }

    /**
     * Get the value of Cogs Account
     *
     * @return integer
     */
    public function getCogsAccount()
    {
        return $this->cogsAccount;
    }

    /**
     * Set the value of Cogs Account
     *
     * @param integer cogsAccount
     *
     * @return self
     */
    public function setCogsAccount($cogsAccount)
    {
        $this->cogsAccount = $cogsAccount;

        return $this;
    }

    /**
     * Get the value of Income Account
     *
     * @return integer
     */
    public function getIncomeAccount()
    {
        return $this->incomeAccount;
    }

    /**
     * Set the value of Income Account
     *
     * @param integer incomeAccount
     *
     * @return self
     */
    public function setIncomeAccount($incomeAccount)
    {
        $this->incomeAccount = $incomeAccount;

        return $this;
    }


    /**
     * Get the value of Unit Of Measure
     *
     * @return string
     */
    public function getUnitOfMeasure()
    {
        return $this->unitOfMeasure;
    }

    /**
     * Set the value of Unit Of Measure
     *
     * @param string unitOfMeasure
     *
     * @return self
     */
    public function setUnitOfMeasure($unitOfMeasure)
    {
        $this->unitOfMeasure = $unitOfMeasure;

        return $this;
    }

    /**
     * Get the value of Track Inventory
     *
     * @return boolean
     */
    public function getTrackInventory()
    {
        return $this->trackInventory;
    }

    /**
     * Set the value of Track Inventory
     *
     * @param boolean trackInventory
     *
     * @return self
     */
    public function setTrackInventory($trackInventory)
    {
        $this->trackInventory = $trackInventory;

        return $this;
    }

}
