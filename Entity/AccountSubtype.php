<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountSubtype
 *
 * @ORM\Table(name="manuf_account_subtype")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\AccountSubtypeRepository")
 */
class AccountSubtype
{

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\AccountType", inversedBy="subtypes")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $superType;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $series;

    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\Account", mappedBy="accountType")
     * @ORM\JoinColumn(nullable=true)
     */
    private $accounts;

    public function __construct()
    {
        $this->accounts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }



    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Super Type
     *
     * @return string
     */
    public function getSuperType()
    {
        return $this->superType;
    }

    /**
     * Set the value of Super Type
     *
     * @param string superType
     *
     * @return self
     */
    public function setSuperType($superType)
    {
        $this->superType = $superType;

        return $this;
    }

    /**
     * Get the value of Series
     *
     * @return integer
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Set the value of Series
     *
     * @param integer series
     *
     * @return self
     */
    public function setSeries($series)
    {
        $this->series = $series;

        return $this;
    }

    /**
     * Get the value of Accounts
     *
     * @return mixed
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * Set the value of Accounts
     *
     * @param mixed accounts
     *
     * @return self
     */
    public function setAccounts($accounts)
    {
        $this->accounts = $accounts;

        return $this;
    }

}
