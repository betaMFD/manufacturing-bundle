<?php
namespace BetaMFD\ManufacturingBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

use BetaMFD\ManufacturingBundle\Entity\Account;
use BetaMFD\ManufacturingBundle\Entity\GeneralLedgerEntry;
use BetaMFD\ManufacturingBundle\Entity\Item;
use BetaMFD\ManufacturingBundle\Entity\Vendor;

/**
 * GeneralLedgerRow
 *
 * @ORM\Table(name="manuf_general_ledger_row")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\GeneralLedgerRowRepository")
 */
class GeneralLedgerRow
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\GeneralLedgerEntry", inversedBy="glRows")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $glEntry;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $memo;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Account", inversedBy="glRows")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Vendor", inversedBy="glRows")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $vendor;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Item", inversedBy="glRows")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $item;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Inventory", inversedBy="glRows")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $inventory;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=18, scale=2, nullable=true)
     */
    private $dr;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=18, scale=2, nullable=true)
     */
    private $cr;

    public function __construct(
        GeneralLedgerEntry $gl_entry = null,
        Account $account = null,
        $dr = 0,
        $cr = 0,
        $reference = null,
        $memo = null,
        Vendor $vendor = null,
        Item $item = null
    ) {
        $this->glEntry = $gl_entry;
        $this->account = $account;
        $this->dr = $dr;
        $this->cr = $cr;
        $this->reference = $reference;
        $this->memo = $memo;
        $this->vendor = $vendor;
        $this->item = $item;
    }


    public function __toString()
    {
        return "GL Row $this->id";
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param integer id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Gl Entry
     *
     * @return integer
     */
    public function getGlEntry()
    {
        return $this->glEntry;
    }

    /**
     * Set the value of Gl Entry
     *
     * @param integer glEntry
     *
     * @return self
     */
    public function setGlEntry($glEntry)
    {
        $this->glEntry = $glEntry;

        return $this;
    }

    /**
     * Get the value of Reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set the value of Reference
     *
     * @param string reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get the value of Memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set the value of Memo
     *
     * @param string memo
     *
     * @return self
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * Get the value of Account
     *
     * @return integer
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set the value of Account
     *
     * @param integer account
     *
     * @return self
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get the value of Vendor
     *
     * @return string
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * Set the value of Vendor
     *
     * @param string vendor
     *
     * @return self
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * Get the value of Item
     *
     * @return string
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set the value of Item
     *
     * @param string item
     *
     * @return self
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }


    /**
     * Get the value of Dr
     *
     * @return string
     */
    public function getDr()
    {
        return $this->dr;
    }

    /**
     * Set the value of Dr
     *
     * @param string dr
     *
     * @return self
     */
    public function setDr($dr)
    {
        $this->dr = $dr;

        return $this;
    }

    /**
     * Get the value of Cr
     *
     * @return string
     */
    public function getCr()
    {
        return $this->cr;
    }

    /**
     * Set the value of Cr
     *
     * @param string cr
     *
     * @return self
     */
    public function setCr($cr)
    {
        $this->cr = $cr;

        return $this;
    }

}
