<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vendor
 *
 * @ORM\Table(name="manuf_vendor")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\VendorRepository")
 */
class Vendor
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow", mappedBy="vendor")
     * @ORM\JoinColumn(nullable=true)
     */
    private $glRows;

    public function __construct()
    {
        $this->glRows = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Gl Rows
     *
     * @return mixed
     */
    public function getGlRows()
    {
        return $this->glRows;
    }

    /**
     * Set the value of Gl Rows
     *
     * @param mixed glRows
     *
     * @return self
     */
    public function setGlRows($glRows)
    {
        $this->glRows = $glRows;

        return $this;
    }

}
