<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipment
 *
 * @ORM\Table(name="manuf_equipment")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\EquipmentRepository")
 */
class Equipment
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $purchaseDate;

    //Can have multiple relating entries
    //ManyToMany as one entry can theoretically affect multiple equipments
    // and one equipment can definitely have multiple gl entries
    /**
     * @ORM\ManyToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\GeneralLedgerEntry", mappedBy="equipments")
     */
    private $glEntries;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=18, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $depreciation = false; //always off while we're not a business

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $modelNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $manual;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    public function __construct()
    {
        $this->glEntries = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function __toString()
    {
        return $this->name;
    }



    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Purchase Date
     *
     * @return \DateTime
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * Set the value of Purchase Date
     *
     * @param \DateTime purchaseDate
     *
     * @return self
     */
    public function setPurchaseDate(\DateTime $purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    /**
     * Get the value of Gl Entries
     *
     * @return mixed
     */
    public function getGlEntries()
    {
        return $this->glEntries;
    }

    /**
     * Set the value of Gl Entries
     *
     * @param mixed glEntries
     *
     * @return self
     */
    public function setGlEntries($glEntries)
    {
        $this->glEntries = $glEntries;

        return $this;
    }

    /**
     * Get the value of Amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the value of Amount
     *
     * @param string amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get the value of Depreciation
     *
     * @return boolean
     */
    public function getDepreciation()
    {
        return $this->depreciation;
    }

    /**
     * Set the value of Depreciation
     *
     * @param boolean depreciation
     *
     * @return self
     */
    public function setDepreciation($depreciation)
    {
        $this->depreciation = $depreciation;

        return $this;
    }

    /**
     * Get the value of Model Number
     *
     * @return string
     */
    public function getModelNumber()
    {
        return $this->modelNumber;
    }

    /**
     * Set the value of Model Number
     *
     * @param string modelNumber
     *
     * @return self
     */
    public function setModelNumber($modelNumber)
    {
        $this->modelNumber = $modelNumber;

        return $this;
    }

    /**
     * Get the value of Manual
     *
     * @return string
     */
    public function getManual()
    {
        return $this->manual;
    }

    /**
     * Set the value of Manual
     *
     * @param string manual
     *
     * @return self
     */
    public function setManual($manual)
    {
        $this->manual = $manual;

        return $this;
    }

    /**
     * Get the value of Notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set the value of Notes
     *
     * @param string notes
     *
     * @return self
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

}
