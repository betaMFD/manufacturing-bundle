<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountType
 *
 * @ORM\Table(name="manuf_account_type")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\AccountTypeRepository")
 */
class AccountType
{

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=2, nullable=false)
     */
    private $drOrCr;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $series;

    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\AccountSubType", mappedBy="superType")
     * @ORM\JoinColumn(nullable=true)
     */
    private $subtypes;


    public function __construct()
    {
        $this->subtypes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function isDR()
    {
        return $this->$drOrCr == 'DR' ? true : false;
    }

    public function isCR()
    {
        return $this->$drOrCr == 'CR' ? true : false;
    }



    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Dr Or Cr
     *
     * @return string
     */
    public function getDrOrCr()
    {
        return $this->drOrCr;
    }

    /**
     * Set the value of Dr Or Cr
     *
     * @param string drOrCr
     *
     * @return self
     */
    public function setDrOrCr($drOrCr)
    {
        $this->drOrCr = $drOrCr;

        return $this;
    }

    /**
     * Get the value of Series
     *
     * @return integer
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Set the value of Series
     *
     * @param integer series
     *
     * @return self
     */
    public function setSeries($series)
    {
        $this->series = $series;

        return $this;
    }

    /**
     * Get the value of Subtypes
     *
     * @return mixed
     */
    public function getSubtypes()
    {
        return $this->subtypes;
    }

    /**
     * Set the value of Subtypes
     *
     * @param mixed subtypes
     *
     * @return self
     */
    public function setSubtypes($subtypes)
    {
        $this->subtypes = $subtypes;

        return $this;
    }

}
