<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inventory
 *
 * @ORM\Table(name="manuf_inventory")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\InventoryRepository")
 */
class Inventory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\Item", inversedBy="glRows")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $item;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=18, scale=6, nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=18, scale=6, nullable=true)
     */
    private $qty;


    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow", mappedBy="inventory")
     * @ORM\JoinColumn(nullable=true)
     */
    private $glRows;



    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Item
     *
     * @return string
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set the value of Item
     *
     * @param string item
     *
     * @return self
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get the value of Date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date
     *
     * @param \DateTime date
     *
     * @return self
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of Amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the value of Amount
     *
     * @param string amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get the value of Qty
     *
     * @return string
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set the value of Qty
     *
     * @param string qty
     *
     * @return self
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

}
