<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BetaMFD\ManufacturingBundle\Entity\Account;
use BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow;
use BetaMFD\ManufacturingBundle\Entity\Item;
use BetaMFD\ManufacturingBundle\Entity\Vendor;

/**
 * GeneralLedgerEntry
 *
 * @ORM\Table(name="manuf_general_ledger")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\GeneralLedgerEntryRepository")
 */
class GeneralLedgerEntry
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $reference;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $memo;

    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow",
     *  mappedBy="glEntry",
     *  cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $glRows;

    /**
     * Many files have Many batches.
     * @ORM\ManyToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\Equipment", inversedBy="glEntries")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $equipments;

    public function __construct($date = null, $reference = null, $memo = null)
    {
        $this->glRows = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equipments = new \Doctrine\Common\Collections\ArrayCollection();
        if (empty($date)) {
            $date = new \DateTime;
        }
        $this->date = $date;
        $this->reference = $reference;
        $this->memo = $memo;
    }

    public function __toString()
    {
        return "GL Entry $this->id";
    }
    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param integer id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set the value of Reference
     *
     * @param string reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get the value of Date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date
     *
     * @param \DateTime date
     *
     * @return self
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of Memo
     *
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Set the value of Memo
     *
     * @param string memo
     *
     * @return self
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * Get the value of Gl Rows
     *
     * @return mixed
     */
    public function getGlRows()
    {
        return $this->glRows;
    }

    /**
     * Add glRow
     *
     * @param BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow $glRow
     *
     * @return Merchant
     */
    public function addGlRow(GeneralLedgerRow $glRow)
    {
        $this->glRows[] = $glRow;

        return $this;
    }

    /**
     * Remove glRow
     *
     * @param BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow $glRow
     */
    public function removeGlRow(GeneralLedgerRow $glRow)
    {
        $this->glRows->removeElement($glRow);
    }

    public function newRow(
        Account $account = null,
        $amount = null,
        $dr_or_cr = null,
        $reference = null,
        $memo = null,
        Vendor $vendor = null,
        Item $item = null
    ) {
        $row = new GeneralLedgerRow(
            $this,
            $account,
            ($dr_or_cr == 'DR' ? $amount : 0),
            ($dr_or_cr == 'CR' ? $amount : 0),
            $reference,
            $memo,
            $vendor,
            $item
        );
        $this->addGlRow($row);
    }

    /**
     * Get the value of Many files have Many batches.
     *
     * @return mixed
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Add equipment
     *
     * @param BetaMFD\ManufacturingBundle\Entity\Equipment $equipment
     *
     * @return Merchant
     */
    public function addEquipment(Equipment $equipment)
    {
        $this->equipments[] = $equipment;

        return $this;
    }

    /**
     * Remove equipment
     *
     * @param BetaMFD\ManufacturingBundle\Entity\Equipment $equipment
     */
    public function removeEquipment(Equipment $equipment)
    {
        $this->equipments->removeElement($equipment);
    }

}
