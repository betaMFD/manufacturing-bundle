<?php

namespace BetaMFD\ManufacturingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table(name="manuf_account")
 * @ORM\Entity(repositoryClass="BetaMFD\ManufacturingBundle\Repository\AccountRepository")
 */
class Account
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=550, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="BetaMFD\ManufacturingBundle\Entity\AccountSubtype", inversedBy="accounts")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $accountType;

    /**
     * @ORM\OneToMany(targetEntity="BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow", mappedBy="account")
     * @ORM\JoinColumn(nullable=true)
     */
    private $glRows;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $payment = false;


    public function __construct()
    {
        $this->glRows = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param integer id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Account Type
     *
     * @return mixed
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * Set the value of Account Type
     *
     * @param mixed accountType
     *
     * @return self
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get the value of Gl Rows
     *
     * @return mixed
     */
    public function getGlRows()
    {
        return $this->glRows;
    }

    /**
     * Set the value of Gl Rows
     *
     * @param mixed glRows
     *
     * @return self
     */
    public function setGlRows($glRows)
    {
        $this->glRows = $glRows;

        return $this;
    }


    /**
     * Get the value of Payment
     *
     * @return boolean
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set the value of Payment
     *
     * @param boolean payment
     *
     * @return self
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

}
