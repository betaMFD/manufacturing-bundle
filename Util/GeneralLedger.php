<?php

namespace BetaMFD\ManufacturingBundle\Service;

use BetaMFD\ManufacturingBundle\Entity\Account;
use BetaMFD\ManufacturingBundle\Entity\GeneralLedgerEntry;
use BetaMFD\ManufacturingBundle\Entity\GeneralLedgerRow;
use DateTime;

/**
 * This class manages GL entries for display.
 */
class GLCodes
{
    private $codes = [];
    private $dr = [];
    private $cr = [];
    private $memo = [];
    private $dr_total = 0;
    private $cr_total = 0;
    private $normalized = false;
    private $filler_code = '';

    /**
     * Add a Debit row to the GL
     * @param Account $code
     * @param string  $amount
     * @param string  $memo
     */
    public function addDR(Account $code, $amount, $memo = null)
    {
        $this->add($code, $amount, 0, $memo);
    }

    /**
     * Add a Credit row to the GL
     * @param Account $code
     * @param string  $amount
     * @param string  $memo
     */
    public function addCR(Account $code, $amount, $memo = null)
    {
        $this->add($code, 0, $amount, $memo);
    }

    /**
     * Add a row to the GL
     * @param Account $account
     * @param string  $dr      Debit dollar value
     * @param string  $cr      Credit dollar value
     * @param string  $memo
     */
    public function add(Account $account, $dr = 0, $cr = 0, $memo = null)
    {
        $code = $account->getId();
        $this->addAccount($account);

        //add DR
        $this->dr[$code] = bcadd($this->dr[$code], $dr, 4);
        $this->dr_total = bcadd($this->dr_total, $dr, 4);

        //add CR
        $this->cr[$code] = bcadd($this->cr[$code], $cr, 4);
        $this->cr_total = bcadd($this->cr_total, $cr, 4);

        if (!empty($memo) and empty($this->memo[$code])) {
            //new memo
            $this->memo[$code] = $memo;
        } elseif (!empty($memo) and $memo != $this->memo[$code]){
            //if there's already a memo set and it isn't the same as the one given
            $this->memo[$code] .= " \n" . $memo;
        }

        //even if this was already normalized,
        //as soon as we add money, we will need to normalize again
        $this->normalized = false;
    }

    private function addAccount(Account $account)
    {
        $code = $account->getId();
        if (empy($this->codes[$code])) {
            $this->codes[$code] = $account;
            $this->dr[$code] = 0;
            $this->cr[$code] = 0;
            $this->memo[$code] = '';
        }
        return $this;
    }

    /**
     * Normalize the entry by moving negative values to the other side
     * If the DR is negative, it becomes a CR, and
     * if the CR is negative, it becomes a DR.
     * @return self
     */
    public function normalize()
    {
        if ($this->normalized) {
            return $this;
        }
        //make everything postive
        foreach ($this->dr as $code => $amount) {
            //if DR is negative, move it to CR
            if ($amount < 0) {
                $this->dr[$code] = 0;
                $this->cr($code, -$amount);
            }
        }
        foreach($this->cr as $code => $amount) {
            //if CR is negative, move it to DR
            if ($amount < 0) {
                $this->cr[$code] = 0;
                $this->dr($code, -$amount);
            }
        }

        //make sure there isn't both a dr and a cr of the same code
        foreach ($this->dr as $code => $amount) {
            if (!empty($this->dr[$code]) and !empty($this->cr[$code])) {
                //oh no!
                //both dr and cr have an amount for this code!
                $dr = &$this->dr[$code];
                $cr = &$this->cr[$code];
                if ($dr >= $cr) {
                    //dr 100, cr 50, subtract cr from both
                    $this->add($code, -$cr, -$cr);
                } else {
                    //dr 50, cr 100, subtract dr from both
                    $this->add($code, -$dr, -$dr);
                }
            }
        }
        //DR and CR should always match up, so I shouldn't need to loop over CR

        ksort($this->dr);
        ksort($this->cr);
        $this->normalized = true;
        return $this;
    }

    /**
     * Find the amount that's off on this entry and fill it with a given account
     * Will put on DR/CR side as required.
     *
     * @param  Account $code
     * @return self
     */
    public function filler(Account $code)
    {
        //get totals
        $dr_total = $this->getDrTotal();
        $cr_total = $this->getCRTotal();

        if ($dr_total == $cr_total) {
            return $this;
        }
        if ($dr_total > $cr_total) {
            $amount = bcsub($dr_total, $cr_total, 2);
            $this->cr($code, $amount);
        } else {
            $amount = bcsub($cr_total, $dr_total, 2);
            $this->dr($code, $amount);
        }

        $this->filler_code = $code;
        return $this;
    }

    /**
     * Get the value of Dr Total
     *
     * @return mixed
     */
    public function getDrTotal()
    {
        //make sure all the money is where it should be
        $this->normalize();
        return round($this->dr_total, 2);
    }

    /**
     * Get the value of Cr Total
     *
     * @return mixed
     */
    public function getCrTotal()
    {
        //make sure all the money is where it should be
        $this->normalize();
        return round($this->cr_total, 2);
    }

    /**
     * Get the value of Codes
     *
     * @return mixed
     */
    public function getCodes()
    {
        return $this->codes;
    }

    /**
     * Get the value of Dr
     *
     * @return mixed
     */
    public function getDr()
    {
        return $this->dr;
    }

    /**
     * Get the value of Cr
     *
     * @return mixed
     */
    public function getCr()
    {
        return $this->cr;
    }

    /**
     * Get the value of Memo
     *
     * @return mixed
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * Get the value of Normalized
     *
     * @return mixed
     */
    public function getNormalized()
    {
        return $this->normalized;
    }

    /**
     * Get the value of Filler Code
     *
     * @return mixed
     */
    public function getFillerCode()
    {
        return $this->filler_code;
    }

}
